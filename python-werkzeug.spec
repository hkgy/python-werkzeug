%global _empty_manifest_terminate_build 0
Name:		python-werkzeug
Version:	2.3.7
Release:	1
Summary:	The comprehensive WSGI web application library.
License:	BSD-3-Clause
URL:		https://palletsprojects.com/p/werkzeug/
Source0:	https://files.pythonhosted.org/packages/source/W/Werkzeug/Werkzeug-2.3.7.tar.gz
# for test
Source1:        https://github.com/Yelp/ephemeral-port-reserve/blob/master/ephemeral_port_reserve.py

BuildArch:	noarch
BuildRequires:	python3-werkzeug python3-markupsafe

Requires:	python3-pytest
Requires:	python3-pytest-xprocess
Requires:	python3-pytest-timeout
Requires:	python3-coverage
Requires:	python3-tox
Requires:	python3-sphinx
Requires:	python3-pallets-sphinx-themes
Requires:	python3-sphinx-issues
Requires:	python3-watchdog

%description
*werkzeug* German noun: "tool". Etymology: *werk* ("work"), *zeug* ("stuff")
Werkzeug is a comprehensive `WSGI`_ web application library. It began as
a simple collection of various utilities for WSGI applications and has
become one of the most advanced WSGI utility libraries.
It includes:
-   An interactive debugger that allows inspecting stack traces and
    source code in the browser with an interactive interpreter for any
    frame in the stack.
-   A full-featured request object with objects to interact with
    headers, query args, form data, files, and cookies.
-   A response object that can wrap other WSGI applications and handle
    streaming data.
-   A routing system for matching URLs to endpoints and generating URLs
    for endpoints, with an extensible system for capturing variables
    from URLs.
-   HTTP utilities to handle entity tags, cache control, dates, user
    agents, cookies, files, and more.
-   A threaded WSGI server for use while developing applications
    locally.
-   A test client for simulating HTTP requests during testing without
    requiring running a server.
Werkzeug doesn't enforce any dependencies. It is up to the developer to
choose a template engine, database adapter, and even how to handle
requests. It can be used to build all sorts of end user applications
such as blogs, wikis, or bulletin boards.
`Flask`_ wraps Werkzeug, using it to handle the details of WSGI while
providing more structure and patterns for defining powerful
applications.

%package -n python3-werkzeug
Summary:	The comprehensive WSGI web application library.
Provides:	python-werkzeug
BuildRequires:  python3-pytest
BuildRequires:	python3-pytest-xprocess
BuildRequires:  python3-pytest-timeout
BuildRequires:  python3-requests
BuildRequires:	python3-devel
BuildRequires:  python3-cryptography
BuildRequires:  python3-greenlet
BuildRequires:	python3-setuptools
%description -n python3-werkzeug
*werkzeug* German noun: "tool". Etymology: *werk* ("work"), *zeug* ("stuff")
Werkzeug is a comprehensive `WSGI`_ web application library. It began as
a simple collection of various utilities for WSGI applications and has
become one of the most advanced WSGI utility libraries.
It includes:
-   An interactive debugger that allows inspecting stack traces and
    source code in the browser with an interactive interpreter for any
    frame in the stack.
-   A full-featured request object with objects to interact with
    headers, query args, form data, files, and cookies.
-   A response object that can wrap other WSGI applications and handle
    streaming data.
-   A routing system for matching URLs to endpoints and generating URLs
    for endpoints, with an extensible system for capturing variables
    from URLs.
-   HTTP utilities to handle entity tags, cache control, dates, user
    agents, cookies, files, and more.
-   A threaded WSGI server for use while developing applications
    locally.
-   A test client for simulating HTTP requests during testing without
    requiring running a server.
Werkzeug doesn't enforce any dependencies. It is up to the developer to
choose a template engine, database adapter, and even how to handle
requests. It can be used to build all sorts of end user applications
such as blogs, wikis, or bulletin boards.
`Flask`_ wraps Werkzeug, using it to handle the details of WSGI while
providing more structure and patterns for defining powerful
applications.

%package help
Summary:	Development documents and examples for Werkzeug
Provides:	python3-werkzeug-doc
%description help
*werkzeug* German noun: "tool". Etymology: *werk* ("work"), *zeug* ("stuff")
Werkzeug is a comprehensive `WSGI`_ web application library. It began as
a simple collection of various utilities for WSGI applications and has
become one of the most advanced WSGI utility libraries.
It includes:
-   An interactive debugger that allows inspecting stack traces and
    source code in the browser with an interactive interpreter for any
    frame in the stack.
-   A full-featured request object with objects to interact with
    headers, query args, form data, files, and cookies.
-   A response object that can wrap other WSGI applications and handle
    streaming data.
-   A routing system for matching URLs to endpoints and generating URLs
    for endpoints, with an extensible system for capturing variables
    from URLs.
-   HTTP utilities to handle entity tags, cache control, dates, user
    agents, cookies, files, and more.
-   A threaded WSGI server for use while developing applications
    locally.
-   A test client for simulating HTTP requests during testing without
    requiring running a server.
Werkzeug doesn't enforce any dependencies. It is up to the developer to
choose a template engine, database adapter, and even how to handle
requests. It can be used to build all sorts of end user applications
such as blogs, wikis, or bulletin boards.
`Flask`_ wraps Werkzeug, using it to handle the details of WSGI while
providing more structure and patterns for defining powerful
applications.

%prep
%autosetup -n Werkzeug-%{version} -p1
cp %{SOURCE1} %{_builddir}/Werkzeug-%{version}/tests/

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
PYTHONPATH=%{buildroot}%{python3_sitelib} pytest -k 'not (test_serving)'

%files -n python3-werkzeug -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Aug 21 2023 HKGY <kaguyahatu@outlook.com> - 2.3.7-1
- Update to 2.3.7

* Tue May 09 2023 wulei <wu_lei@hoperun.com> - 2.2.3-1
- Update to 2.2.3

* Sat Jan 7 2023 Bolehu <heyaohua@xfusion.com> - 2.0.3-2
- fix typo and grammar mistake

* Fri Jun 17 2022 jiangpengju <jiangpengju2@h-partners.com> - 2.0.3-1
- Upgrade python-werkzeug version to 2.0.3

* Mon Nov 15 2021 xu_ping <xuping33@huawei.com>-1.0.1-2
- fix test failures due to unhandled exceptions being thrown without being propagated to caller.

* Thu Feb 04 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
